use std::cell::RefCell;
use std;

use std::sync::Arc;
use std::sync::RwLock;
use std::rc::Rc;
use std::collections::HashMap;
use analysis::analysis::Chain;

// Sourcable is anything that can be used as a source for a chain.
// For example, mic input is a sourcable and audio file stream is a sourcable.
pub trait Sourcable: Send + Sync {
    fn start(&mut self, chain: Arc<RwLock<Chain>>);
    fn stop(&mut self);
    fn get_devices() -> Result<HashMap<String, (String, i32)>, ()> where Self: Sized;
    fn is_active(&self) -> bool;
    fn get_and_clear_error(&self) -> Option<String>;
    fn get_selected_device(&self) -> String;
}

// Chainable is anything that can be put into a chain as a node for the chain.
pub trait Chainable: Send + Sync {
    fn update(&mut self, buffer: &Vec<Vec<f64>>);
    fn output(&self) -> &Vec<f64>;
}

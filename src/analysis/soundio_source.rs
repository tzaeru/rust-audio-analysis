extern crate soundio;

use std::collections::HashMap;
use analysis::traits::Sourcable;
use analysis::traits::Chainable;
use analysis::analysis::Chain;

use std;

use std::sync::Arc;
use std::sync::RwLock;

// We need soundio context to be globally available, any other approach is currently pretty clumsy. Forgive us.
lazy_static! {
    static ref SOUNDIO_CTX: soundio::Context<'static> = {
        let mut ctx = soundio::Context::new();
        ctx.set_app_name("RAA!");
        ctx.connect().unwrap();
        ctx.flush_events();

        println!("Soundio version: {}", soundio::version_string());
        println!("Current backend: {:?}", ctx.current_backend());
        for mut dev in ctx.input_devices().unwrap() {
            dev.sort_channel_layouts();
            println!("Device {} ", dev.name());
            println!("Is raw: {}", dev.is_raw());
            println!("Formats: {:?}", dev.formats());
            let mut most_channels_layout = 0;
            for i in 0..dev.layouts().len()
            {
                if dev.layouts()[i].channels.len() > dev.layouts()[most_channels_layout].channels.len()
                {
                    most_channels_layout = i;
                }
            }
            println!("Layout w/ most channels: {:?}", dev.layouts()[most_channels_layout]);
        }
        println!("");
        return ctx;
    };
}

pub struct SoundioSource<'a> {
    device: String,

    stream: Option<soundio::InStream<'a>>,

    error: Arc<RwLock<String>>,

    raw: bool
}

impl<'a> SoundioSource<'a> {
    pub fn new (device: String, raw: bool) -> SoundioSource<'a> {
        SoundioSource {
            device: device,

            stream: Option::None,

            error: Arc::new(RwLock::new("".to_string())),

            raw: raw
        }
    }
}

impl<'a> Sourcable for SoundioSource<'a> {
    fn start(&mut self, chain: Arc<RwLock<Chain>>) -> () {

        let error_capture_for_stream_cb = self.error.clone();
        let audio_callback = move |stream: &mut soundio::InStreamReader| {
            // Unleave
            let mut unleaved_buffer:Vec<Vec<f64>> = Vec::new();

            // CHANGING TO UNLEAVING WITH ALL CHANNELS INCLUDED TO SHARE RAW-MODE DEVICES

            // Initialize with empty arrays for each channel
            for _ in 0..stream.channel_count()
            {
                unleaved_buffer.push(Vec::new());
            }
            // Iterate through the whole interleaved buffer, moving it to unleaved buffer.
            let mut frames_left = stream.frame_count_max();
            loop {
                if let Err(e) = stream.begin_read(frames_left) {
                    let mut write_lock = error_capture_for_stream_cb.write().unwrap();
                    *write_lock = e.to_string();
                    println!("Error reading from stream: {}", e);
                    return;
                }

                for f in 0..stream.frame_count() {
                    for ch_i in 0..stream.channel_count() {
                        let mut value = stream.sample::<f64>(ch_i as usize, f);
                        if !value.is_normal()
                        {
                            value = 0.0f64;
                        }
                        if value > 1.0
                        {
                            println!("Value: {}", value);
                        }
                        unleaved_buffer[ch_i].push(value);
                    }
                }

                frames_left -= stream.frame_count();
                if frames_left <= 0 {
                    break;
                }

                stream.end_read();
            }
            match chain.try_write() {
                Ok(lock) => lock.source_cb(unleaved_buffer, stream.frame_count()),
                Err(e) => println!("Error writing to chain: {:?}", e)
            }

            return ();
        };

        let error_capture =  self.error.clone();
        let error_callback = move |error: soundio::Error| {
            println!("Error: {:?}", error);
            let mut write_lock = error_capture.write().unwrap();
            *write_lock = error.to_string();
        };

        let mut soundio_format = soundio::Format::S16LE;
        println!("Going to get default input device..");
        // pa::input<f32>, pa::NonBlocking
        let mut devices = SOUNDIO_CTX.input_devices().unwrap();
        let mut input_dev = &mut SOUNDIO_CTX.default_input_device().map_err(|_| "Error getting default input device".to_string()).unwrap();
        let mut found_dev = false;
        for device in devices.iter_mut()
        {
            // Change default device to raw.. As long as there's formats available
            /*if device.id() == input_dev.id() &&
                ((!device.is_raw() && device.formats().len() > 0) ||
                input_dev.formats().len() <= 0)
            {
                input_dev = device;
                found_dev = true;
            }
            // Change to selected device // it's default*/
            if device.id() == self.device && device.formats().len() > 0 && device.is_raw() == self.raw
            {
                println!("Found a match: {}", device.id());
                input_dev = device;
                found_dev = true;
            }
        }

        if !input_dev.supports_format(soundio::Format::S16LE) && input_dev.formats().len() > 0
        {
            soundio_format = input_dev.formats()[0];
        }

        println!("Using format: {:?}", soundio_format);

        // Report an error for not finding a device matching the desired device ID
        if found_dev == false
        {
            println!("Device not found, wanted to find: {:?}", self.device);
            let mut write_lock = self.error.write().unwrap();
            *write_lock = "Device not found!".to_string();
            return;
        }

        let sample_rate = input_dev.nearest_sample_rate(44100);

        input_dev.sort_channel_layouts();
        println!("Got default input device: {:?}", input_dev.name());
        println!("Aim: {:?}", input_dev.aim());
        println!("Raw: {:?}", input_dev.is_raw());
        println!("Formats: {:?}", input_dev.formats());
        println!("Sample rates: {:?}", input_dev.sample_rates());
        let mut most_channels_layout = 0;
        for i in 0..input_dev.layouts().len()
        {
            if input_dev.layouts()[i].channels.len() > input_dev.layouts()[most_channels_layout].channels.len()
            {
                most_channels_layout = i;
            }
        }
        let layout = input_dev.layouts()[most_channels_layout].clone();
        println!("Layout: {:?}", input_dev.layouts()[most_channels_layout]);
        let mut stream = input_dev.open_instream(
            sample_rate,
            soundio_format,
            layout,
            (1.0/20.0 * sample_rate as f64) / sample_rate as f64,
            audio_callback,
            None::<fn()>,
            Some(error_callback),
        ).unwrap();
        println!("Starting soundio stream..");

        stream.start().unwrap();

        println!("Bytes per sample: {}", stream.bytes_per_sample());

        self.stream = Option::Some(stream);
    }

    fn stop(&mut self) -> () {
        println!("Stopping SoundIO source.");
        match self.stream
        {
            Some(ref mut stream) => stream.pause(true).unwrap(),
            None => return
        }
        println!("Stopped SoundIO Source!");
    }

    fn is_active(&self) -> bool
    {
        match self.stream
        {
            Some(ref stream) => true,
            None => return false
        }
    }

    fn get_devices() -> Result<HashMap<String, (String, i32)>, ()> where Self: Sized
    {
        let mut devices = HashMap::new();

        for mut dev in SOUNDIO_CTX.input_devices().unwrap() {
            let mut most_channels_layout = 0;
            for i in 0..dev.layouts().len()
            {
                if dev.layouts()[i].channels.len() > dev.layouts()[most_channels_layout].channels.len()
                {
                    most_channels_layout = i;
                }
            }
            devices.insert(dev.id(), (dev.name(), dev.layouts()[most_channels_layout].channels.len() as i32));
        }

        return Ok(devices);
    }

    fn get_and_clear_error(&self) -> Option<String>
    {
        let error_clone = self.error.read().unwrap().clone();
        if error_clone.len() <= 0
        {
            return None;
        }
        *self.error.write().unwrap() = "".to_string();

        return Some(error_clone);
    }

    fn get_selected_device(&self) -> String
    {
        return self.device.to_owned();
    }
}

impl<'a> Drop for SoundioSource<'a> {
    fn drop(&mut self) {
        println!("Dropping SoundioSource!");
    }
}

unsafe impl<'a> Send for SoundioSource<'a> {}
unsafe impl<'a> Sync for SoundioSource<'a> {}
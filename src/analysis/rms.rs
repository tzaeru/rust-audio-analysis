use analysis::traits::Chainable;

// Calculates the root mean square from audio input
pub struct RMS {
    pub channels: Vec<i32>,
    buffer: Vec<f64>,
}

impl RMS {
    pub fn new() -> RMS {
        RMS { channels: Vec::new(), buffer: Vec::new() }
    }
}

impl Chainable for RMS {
    fn update(&mut self, buffer: &Vec<Vec<f64>>) {

        let mut rms = 0f64;
        for ch_i in &self.channels
        {
            let mut square_sum = 0.0f64;

            // TODO: This should not really be possible, but atm networked client can enable non-existing channels.
            if (buffer.len() <= *ch_i as usize)
            {
                continue;
            }

            for x in 0..buffer[*ch_i as usize].len() {
                square_sum += buffer[*ch_i as usize][x] * buffer[*ch_i as usize][x];
                /*if x%10000 == 0
                {
                    println!("Buffer value: {:?}",  buffer[*ch_i as usize][x]);
                }*/
            }

            let square_mean = square_sum / buffer[*ch_i as usize].len() as f64;

            rms += f64::sqrt(square_mean);
        }
/*
        for i in 0..buffer.len()
        {
            let mut square_sum = 0.0f32;
            for x in 0..buffer[i].len() {
                square_sum += buffer[i][x] * buffer[i][x];
            }

            let square_mean = square_sum * 1.0f32 / buffer.len() as f32;

            rms += f32::sqrt(square_mean);
        }*/

        rms /= self.channels.len() as f64;

        self.buffer = Vec::new();
        self.buffer.push(rms);
    }

    fn output(&self) -> &Vec<f64> {
        &self.buffer
    }
}

unsafe impl Send for RMS {}
use std::net::{TcpListener, TcpStream};

use std::io::prelude::*;
use std::{thread, time};

use std::time::{Duration, Instant};

mod messages;

use messages::Serializable;
use messages::MsgType;

extern crate raa;
use raa::analysis;
use raa::analysis::traits::Sourcable;

use std::sync::Arc;
use std::sync::RwLock;


fn handle_client(stream: TcpStream) {
    // ...
}

fn send_devices(mut stream: &TcpStream) -> Result<usize, std::io::Error>
{
    let mut devices = analysis::soundio_source::SoundioSource::get_devices();
    let mut device_msg = messages::MsgDevicesList::new();
    device_msg.devices = devices.unwrap();

    let mut serialized = device_msg.serialize();
    stream.write(serialized.as_mut_slice())
}

fn send_test_error(mut stream: &TcpStream) -> Result<usize, std::io::Error>
{
    let mut error_msg = messages::MsgError::new();
    error_msg.message = "horror".to_string();

    let mut serialized = error_msg.serialize();
    stream.write(serialized.as_mut_slice())
}

fn send_error(mut stream: &TcpStream, error: String) -> Result<usize, std::io::Error>
{
    let mut error_msg = messages::MsgError::new();
    error_msg.message = error;

    let mut serialized = error_msg.serialize();
    stream.write(serialized.as_mut_slice())
}

fn send_rms_msg(mut stream: &TcpStream, rms: f64) -> Result<usize, std::io::Error>
{
    let mut rms_msg = messages::MsgRMSPacket::new();
    rms_msg.value = rms;

    let mut serialized = rms_msg.serialize();
    stream.write(serialized.as_mut_slice())
}

fn client_handler(mut stream: std::net::TcpStream, chains: Arc<RwLock<Vec<Arc<RwLock<raa::analysis::analysis::Chain>>>>>, arena_rc: Arc<RwLock<raa::analysis::analysis::Arena>>)
{
    thread::spawn(move || {
        let _ = send_devices(&stream);
        //let _ = send_test_error(&stream);

        // Ready an analysis chain to be used later on after a proper message has been received.
        let mut chain = analysis::analysis::Chain::new(arena_rc.clone());
        let mut chain_ref = Arc::new(RwLock::new(chain));

        let mut rms_id = None;

        let mut source_id = None;

        let mut send_rms = false;

        // Cap to 20 outgoing messages per second
        let mut sent_msg_instant = Instant::now();

        // Buffer for whole message.
        // Each message is prefixed by message length.
        // As TCP is a streaming protocol, message size may vary.
        // As such, only handle (and remove) a message when whole message is read.
        let mut msg_buffer: Vec<u8> = Vec::new();

        // Read stuff until error.
        loop {
                let mut data = [0u8; 2048];
                match stream.read(&mut data)
                {
                    Ok(read_bytes) => {
                    	// Continue if there was nothing to read
                        if read_bytes <= 0
                        {
                            continue;
                        }

                        // Add read stuff to a buffer
                        for i in 0..read_bytes
                        {
                            msg_buffer.push(data[i]);
                        }

                        // Buffer less than 4 bytes can not contain a message (first bytes are the message length)
                        if msg_buffer.len() < 4
                        {
                            continue;
                        }

                        // Length that the message should have
                        let mut msg_length: i32 = msg_buffer[0] as i32 | ((msg_buffer[1] as i32) << 8) | ((msg_buffer[2] as i32)  << 16) | ((msg_buffer[3] as i32) << 24);

                        // We have a full length message if buffer has msg_length bytes
                        while msg_buffer.len() >= msg_length as usize
                        {
                            // Remove the message's worth of bytes from the buffer.
                            let mut message_bytes: Vec<u8> = msg_buffer.drain(0..msg_length as usize).collect();
                            let msg_type = message_bytes[4] as i32 | ((message_bytes[5] as i32) << 8) | ((message_bytes[6] as i32)  << 16) | ((message_bytes[7] as i32) << 24);

                            if msg_type == MsgType::MSG_GET_RMS as i32
                            {
                                // Check if we already have a RMS node in use (rms_id is set)
                                // If we do, remove the node and create a new one.
                                match rms_id
                                {
                                    Some(id) => {
                                        chain_ref.write().unwrap().remove_node(rms_id.unwrap());
                                        arena_rc.write().unwrap().remove_chainable(rms_id.unwrap());

                                        if chain_ref.read().unwrap().nodes.len() <= 0
                                        {
                                            chain_ref.write().unwrap().stop();
                                            arena_rc.write().unwrap().remove_sourcable(source_id.unwrap());

                                            let mut chains = chains.write().unwrap();

                                            for i in 0..chains.len() {
                                                if Arc::ptr_eq(&chains[i], &chain_ref)
                                                {
                                                    println!("Removing chain {:?}", i);
                                                    chains.remove(i);
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    None => ()
                                }


                                // Remove length and type bytes.
                                let msg_length = message_bytes.len();
                                let messages_bytes_without_type = message_bytes.drain(8..msg_length).collect();
                                println!("Creating RMS msg..");
                                let rms_msg = messages::MsgStartStreamRMS::deserialized(messages_bytes_without_type);
                                println!("Sources count: {:?}", arena_rc.read().unwrap().sourcables.len());
                                println!("Chainables count: {:?}", arena_rc.read().unwrap().chainables.len());
                                println!("Chains count: {:?}", chains.read().unwrap().len());

                                let mut rms = Arc::new(RwLock::new(analysis::rms::RMS::new()));
                                rms.write().unwrap().channels = rms_msg.channels;
                                rms_id = Some(arena_rc.write().unwrap().add_chainable(rms));
                                println!("Created a new RMS node.");

                                let mut found = false;
                                // Check if there already exists a chain for this connection with the audio device in use that the client wanted
                                for _chain in chains.write().unwrap().iter() {
                                    println!("Checking for selected device..");
                                    println!("ID: {:?}", &_chain.read().unwrap().source.unwrap());
                                    println!("Arena borrow source count: {}", arena_rc.read().unwrap().sourcables.len());
                                    if !arena_rc.read().unwrap().sourcables.contains_key(&_chain.read().unwrap().source.unwrap())
                                    {
                                        continue;
                                    }
                                    let mut selected_device = "".to_string();
                                    {
                                        let arena_borrow = arena_rc.read().unwrap();
                                        selected_device = arena_borrow.sourcables[&_chain.read().unwrap().source.unwrap()].read().unwrap().get_selected_device();
                                    }
                                    if selected_device == rms_msg.device_id
                                    {
                                        println!("Found match");
                                        found = true;
                                        source_id = Some(_chain.read().unwrap().source.unwrap());
                                        _chain.write().unwrap().add_node(rms_id.unwrap());
                                        chain_ref = _chain.clone();
                                        break;
                                    }
                                }
                                // If the selected audio device wasn't in use in some chain already, create a new chain
                                if !found
                                {
                                    let source = Arc::new(RwLock::new(analysis::soundio_source::SoundioSource::new(rms_msg.device_id, rms_msg.raw)));
                                    source_id = Some(arena_rc.write().unwrap().add_sourcable(source));
                                    chain = analysis::analysis::Chain::new(arena_rc.clone());
                                    chain.set_source(source_id.unwrap());
                                    chain.add_node(rms_id.unwrap());
                                    chain_ref = Arc::new(RwLock::new(chain));
                                    chains.write().unwrap().push(chain_ref.clone());
                                    println!("Starting chain.");
                                    chain_ref.write().unwrap().start(chain_ref.clone());
                                }
                                print!("\n");
                                send_rms = true;
                            }

                            if msg_buffer.len() >= 4
                            {
                                msg_length = msg_buffer[0] as i32 | ((msg_buffer[1] as i32) << 8) | ((msg_buffer[2] as i32)  << 16) | ((msg_buffer[3] as i32) << 24);
                            }
                        }
                    },
                    Err(e) => {
                        match e.kind() {
                            std::io::ErrorKind::WouldBlock => {},
                            _ => {
                                println!("Some awful error happened: {:?}", e);
                                break;
                                //println!("Breaking.");
                                //arena_rc.write().unwrap().remove_sourcable(source_id.unwrap());
                                //arena_rc.write().unwrap().remove_chainable(rms_id);
                                //break;
                            },
                        }
                    }
                }
                
                let elapsed_as_mills = sent_msg_instant.elapsed().as_secs() * 1000
                                + sent_msg_instant.elapsed().subsec_nanos() as u64 / 1000000;
                // If source has been set, check for errors
                match source_id
                {
                    Some(id) =>
                    {
                        let arena_borrow = arena_rc.read().unwrap();
                        let sourcable = arena_borrow.sourcables[&id].read().unwrap();
                        
                        match sourcable.get_and_clear_error()
                        {
                            Some(error) => {
                                let _ = send_error(&stream, error);
                            }
                            None => ()
                        }
                    },
                    None => ()
                }

          		// If we've succesfully initialized, check for RMS messages to send
                if send_rms == true && elapsed_as_mills > 1000/20
                {
                    sent_msg_instant = Instant::now();

                    let mut chainable_len = 0;
                    {
                        let arena_borrow = arena_rc.read().unwrap();
                        chainable_len = arena_borrow.chainables[&rms_id.unwrap()].read().unwrap().output().len();
                    }

                    if chainable_len > 0
                    {
                        let mut error = Ok(0);
                        {
                            let arena_borrow = arena_rc.read().unwrap();
                            error = send_rms_msg(&stream, arena_borrow.chainables[&rms_id.unwrap()].read().unwrap().output()[0]);
                        }
                        match error
                        {
                            Ok(_) => (),
                            Err(e) => {
                                println!("Connection lost: {:?}", e);
                                break;
                                // Here was stuff.
                            }
                        }
                    }
                }

                let ten_millis = time::Duration::from_millis(30);
                thread::sleep(ten_millis);
        }
        println!("Stopped looping for data - client disconnected?");

        // Clean up (client disconnected)
        chain_ref.write().unwrap().remove_node(rms_id.unwrap());
        arena_rc.write().unwrap().remove_chainable(rms_id.unwrap());
        if chain_ref.read().unwrap().nodes.len() <= 0
        {
            chain_ref.write().unwrap().stop();
            arena_rc.write().unwrap().remove_sourcable(source_id.unwrap());

            let mut chains = chains.write().unwrap();

            for i in 0..chains.len() {
                if Arc::ptr_eq(&chains[i], &chain_ref)
                {
                    println!("Removing chain {:?}", i);
                    chains.remove(i);
                    break;
                }
            }

        }
        //chain_ref.write().unwrap().stop();
    });
}

fn main() {
    let listener = TcpListener::bind("127.0.0.1:50000").unwrap();
    listener.set_nonblocking(true).expect("Cannot set non-blocking");

    let mut chains: Arc<RwLock<Vec<Arc<RwLock<raa::analysis::analysis::Chain>>>>> = Arc::new(RwLock::new(Vec::new()));

    let arena = analysis::analysis::Arena::new();
    let mut arena_rc = Arc::new(RwLock::new(arena));

    loop {
        match listener.accept() {
            Ok((mut stream, addr)) => {
            	println!("new client: {:?}", addr);
                let _ = stream.set_nodelay(true);
                let chains = chains.clone();
    			let arena_rc = arena_rc.clone();
                client_handler(stream, chains, arena_rc);
            },
            Err(e) => {
                //println!("Couldn't accept client connection, {:?}", e);
            },
        }
        let ten_millis = time::Duration::from_millis(20);
        thread::sleep(ten_millis);
    }
}
